# this python scripte performs a loop on the number of  
# single shots used in the supervised regression of 
# observables from stacks single-shot images


#########################################################
#########                 IMPORTS               #########
#########################################################
import sys
sys.path.insert(1, './source/')
print(open('./images/Uniqorn_logo.txt', 'r').read())

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from Models import clear as clearmodels
#import other custom-made modules and classes
import Input as inp 
import DataGenerator
import DataLoading
import Runtime_check          
Runtime_check.runtime_checks()
del Runtime_check

#########################################################
def SpaceString(selector):
  if (selector==True):
    return 'K'
  else:
    return 'X'

histories=[] # array to collect history objects for all trainings
Noises=np.arange(0, 1.05 , 0.5, dtype=float) # range for {N}umber of {S}ingle shots {P}er {S}ample
ImageNoiseScale=10
scores=[] # array to collect model errors
count=0

for i in Noises:
  # modify inputs
  inp.Learn='DENS'
  inp.momLabels=False
  inp.momData=False
  inp.DetectionNoise=True
  inp.DetectionNoiseOnLabels=True
  inp.DetectionNoiseStrength=i
  inp.ImageNoise=False
  inp.ImageNoiseOnLabels=False
  inp.ImageNoiseStrength=i*ImageNoiseScale
  inp.DetectionNoiseWidth=1.0

  DataString=SpaceString(inp.momData)
  LabelString=SpaceString(inp.momLabels)

  # assemble file name
  fname='Errors_'+inp.Learn+'_in_'+LabelString
  if (inp.DetectionNoise):
    if (inp.DetectionNoiseOnLabels):
      fname=fname+'_with_DetectionNoise'
  if (inp.ImageNoise):
    if (inp.ImageNoiseOnLabels):
      fname=fname+'_with_ImageNoise'

  fname=fname+'_from_SSS_in_'+DataString
  if (inp.DetectionNoise):
    fname=fname+'_with_DetectionNoise'
  if (inp.ImageNoise):
    fname=fname+'_with_ImageNoise'

  inp.NShotsPerSample=20   # 20 and 50
  inp.NShots=4*inp.NShotsPerSample
  inp.Loss='mse'

  # Instantiation of the data generators for batch processing
  # with the paths to the files used as data.
  # This construction is needed to load the data and labels on the fly,
  # to not overload the memory for very large data sets.
  if inp.batch_loading==True:
    training_generator, validation_generator, test_generator, y_test = DataGenerator.InitializeDataGenerators()
  elif inp.batch_loading==False:
    training_generator, validation_generator, test_generator, X_train, y_train, X_val, y_val, X_test, y_test = DataGenerator.InitializeDataGenerators()

  
  y_test=np.array(y_test)
  ##################################################################################################################

  # import modules (to update dependencies on modified parameters from input)  
  import ModelTrainingAndValidation

  
  if inp.batch_loading==True:
      (model, 
      history, 
      test_predictions,y_test) = ModelTrainingAndValidation.main(training_generator,
                                                                validation_generator,
                                                                test_generator,
                                                                y_test)
      
  elif inp.batch_loading==False:
      (model, 
      history, 
      test_predictions,y_test) = ModelTrainingAndValidation.main(X_train,  
                                                                 y_train, 
                                                                 X_val,
                                                                 y_val,
                                                                 X_test,
                                                                 y_test)
                                                     
  # append test results to scores arrays
  best_val_loss_index=np.argmin(np.array(history.history['val_loss']))
  print('best validation loss at epoch '+ str(best_val_loss_index))
  print(str(Noises[0:count])) 
  print("shape of Noises:"+str(np.shape(Noises[0:count])))
  score=np.reshape(model.evaluate(test_generator, verbose=0),(1,-1))
  score=np.insert(score,0,Noises[count],axis=1)
  print('score: ' + str(score)+' best losses'+ str(np.float(history.history['loss'][best_val_loss_index]))+ str(np.float(history.history['val_loss'][best_val_loss_index])))
  score=np.insert(score,score.size-1,np.float(np.array(history.history['loss'][best_val_loss_index])),axis=1)
  score=np.insert(score,score.size-1,np.float(np.array(history.history['val_loss'])[best_val_loss_index]),axis=1)
  score=np.insert(score,score.size-1,np.float(inp.ImageNoiseStrength),axis=1)
  score=np.insert(score,score.size-1,np.float(inp.DetectionNoiseStrength),axis=1)
  print('completed score: ' + str(score))

  print("shape of score:"+str(np.shape(score)))
  scores.append(score)
  print("shape of scores:"+str(np.shape(scores)))
  
#  np.savetxt('Errors_'+inp.Learn+'.txt',np.reshape(scores,(-1,6)))
  np.savetxt(fname+'.txt',np.squeeze(scores,axis=1))

  # increment loop counter
  count=count+1
  clearmodels()
  
# reshape scores for plotting
scores=np.squeeze(scores,axis=1)

# create plot of mean standard error and mean absolute error
legend=[]
fig, ax = plt.subplots()
ax.plot(Noises,scores[:,3])
legend.append('loss')
ax.plot(Noises,scores[:,4])
legend.append('val_loss')
ax.plot(Noises,scores[:,1])
legend.append('test_loss')
ax.legend(legend, loc='best')

ax.set(xlabel='Noise', ylabel='Error')
ax.grid()

fig.savefig(fname+".png")

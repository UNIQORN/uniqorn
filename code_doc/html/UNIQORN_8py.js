var UNIQORN_8py =
[
    [ "durationDataLoading", "UNIQORN_8py.html#ae9791de1c55c231b901b876527a18f25", null ],
    [ "durationTotal", "UNIQORN_8py.html#a8c7946061d05f13f921d9885f10e95d3", null ],
    [ "durationTraining", "UNIQORN_8py.html#a040d693d7a80e1585a63583dc41ea6e6", null ],
    [ "durationVisualization", "UNIQORN_8py.html#a02784ec0d8f37baa34cca204d5ee90a7", null ],
    [ "endTime", "UNIQORN_8py.html#a3da27754eba5ce1541aebab755d45b7d", null ],
    [ "pathToSource", "UNIQORN_8py.html#a46d31247d5fe47b2b39c3c5d42294725", null ],
    [ "postLoadTime", "UNIQORN_8py.html#a18b29c0a7c53886e608accb9c48292eb", null ],
    [ "postTrainTime", "UNIQORN_8py.html#a8556dc802662de26aa4b8cf7f8c88166", null ],
    [ "score", "UNIQORN_8py.html#a178ac220ad8bb4575955fff320145873", null ],
    [ "SourceDir", "UNIQORN_8py.html#a2dcbe992200535e949e813f7b4c56dee", null ],
    [ "startTime", "UNIQORN_8py.html#ad2d04d2e20858d92c0b91df15891c21b", null ],
    [ "test_generator", "UNIQORN_8py.html#affee0cd2b16614bb2ba00e75de4732c7", null ],
    [ "test_predictions", "UNIQORN_8py.html#a4a70342dd222924f50389126ef425390", null ],
    [ "training_generator", "UNIQORN_8py.html#a838898c77b2c8e11d5b438fd99d075f9", null ],
    [ "validation_generator", "UNIQORN_8py.html#a039ee79768acaa68705d96cd482301c3", null ],
    [ "X_test", "UNIQORN_8py.html#ab6332a9cc1ecf017e8f4e60b8dc540e2", null ],
    [ "X_train", "UNIQORN_8py.html#a9419b44f0ccc1502ce35c6f21a477ae2", null ],
    [ "X_val", "UNIQORN_8py.html#a5b314efdbe9ff9000e5a47e27b7161bd", null ],
    [ "y_test", "UNIQORN_8py.html#afec07af0a0c13ce3e815e0b464144cf0", null ],
    [ "y_train", "UNIQORN_8py.html#ac584dce3a379a22828eb9fa605e4c8de", null ],
    [ "y_val", "UNIQORN_8py.html#afcde9d310a2ea6bd896f6ceb60b37fe1", null ]
];
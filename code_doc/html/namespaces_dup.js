var namespaces_dup =
[
    [ "Introduction", "index.html#autotoc_md1", null ],
    [ "Prerequisites", "index.html#autotoc_md2", null ],
    [ "Structure of the repository", "index.html#autotoc_md3", null ],
    [ "Quickstart tutorial", "index.html#autotoc_md4", null ],
    [ "Running the code for a single set of hyperparameters", "index.html#autotoc_md5", null ],
    [ "Performing a hyperparameter optimization with HPBandster", "index.html#autotoc_md6", null ],
    [ "DataGenerator", "namespaceDataGenerator.html", null ],
    [ "DataLoading", "namespaceDataLoading.html", null ],
    [ "DataPreprocessing", "namespaceDataPreprocessing.html", null ],
    [ "Error_from_formula", "namespaceError__from__formula.html", null ],
    [ "functions", "namespacefunctions.html", null ],
    [ "HyperParameterOpt", "namespaceHyperParameterOpt.html", null ],
    [ "HyperParameterOpt_Worker", "namespaceHyperParameterOpt__Worker.html", null ],
    [ "Models", "namespaceModels.html", null ],
    [ "ModelTrainingAndValidation", "namespaceModelTrainingAndValidation.html", null ],
    [ "Output", "namespaceOutput.html", null ],
    [ "PlotInteractiveHyperParameterOpt", "namespacePlotInteractiveHyperParameterOpt.html", null ],
    [ "Regression_Loop_Noise", "namespaceRegression__Loop__Noise.html", null ],
    [ "Regression_Loop_NShots", "namespaceRegression__Loop__NShots.html", null ],
    [ "rel_phase_package", "namespacerel__phase__package.html", null ],
    [ "Runtime_check", "namespaceRuntime__check.html", null ],
    [ "test_interpolate_spline", "namespacetest__interpolate__spline.html", null ],
    [ "UNIQORN", "namespaceUNIQORN.html", null ],
    [ "Visualization", "namespaceVisualization.html", null ],
    [ "VisualizationNew", "namespaceVisualizationNew.html", null ]
];
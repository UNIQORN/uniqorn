var searchData=
[
  ['validation_5fgenerator_262',['validation_generator',['../namespaceError__from__formula.html#a5f4f40c024fac5f754d91748cae32910',1,'Error_from_formula.validation_generator()'],['../namespaceRegression__Loop__Noise.html#abd3f2b652ebd325d213c992b43160427',1,'Regression_Loop_Noise.validation_generator()'],['../namespaceRegression__Loop__NShots.html#a0c630385bb7a0e2be530ad19d5f18b92',1,'Regression_Loop_NShots.validation_generator()'],['../namespaceUNIQORN.html#a039ee79768acaa68705d96cd482301c3',1,'UNIQORN.validation_generator()']]],
  ['visualization_263',['Visualization',['../namespaceVisualization.html',1,'']]],
  ['visualization_2epy_264',['Visualization.py',['../Visualization_8py.html',1,'']]],
  ['visualizationnew_265',['VisualizationNew',['../namespaceVisualizationNew.html',1,'']]],
  ['visualizationnew_2epy_266',['VisualizationNew.py',['../VisualizationNew_8py.html',1,'']]],
  ['vizgrid_267',['VizGrid',['../namespaceDataLoading.html#ac14885f9df3443baae8f76a9db744ac7',1,'DataLoading.VizGrid()'],['../namespaceVisualization.html#a73c79fcc95b0f60baca2dc17a37af9b5',1,'Visualization.VizGrid()'],['../namespaceVisualizationNew.html#ac411be80979cbf362dca1d118029d653',1,'VisualizationNew.VizGrid()']]],
  ['vizgrid_2etxt_268',['VizGrid.txt',['../VizGrid_8txt.html',1,'']]]
];

var searchData=
[
  ['plot_5fhistory_403',['plot_history',['../namespaceVisualization.html#a00bf2b5120be3ffd0bfdbc8a4a23fb19',1,'Visualization.plot_history()'],['../namespaceVisualizationNew.html#a016b84ec56bec4832f310f4eaf45795b',1,'VisualizationNew.plot_history()']]],
  ['plot_5finterpol_404',['plot_interpol',['../namespaceModelTrainingAndValidation.html#aa64a3f5e2418adb84853e460315f5d70',1,'ModelTrainingAndValidation']]],
  ['plot_5fphase_5fhistogram_405',['plot_phase_histogram',['../namespacerel__phase__package.html#abd1b230c5f6ce61841adc06094117552',1,'rel_phase_package']]],
  ['plot_5fpredictions_406',['plot_predictions',['../namespaceVisualization.html#a46c754a40fed8328eea0d06b260e42ab',1,'Visualization.plot_predictions()'],['../namespaceVisualizationNew.html#a9d7cd0fe020fb9205b9af7e4653308f9',1,'VisualizationNew.plot_predictions()']]],
  ['plotmodel_407',['PlotModel',['../namespaceOutput.html#ac7173aaf8e4f9a49b59301228ff83fdf',1,'Output']]],
  ['pmodel_408',['Pmodel',['../namespacerel__phase__package.html#a84c456c236449af1bda2d8448a120bbd',1,'rel_phase_package']]]
];

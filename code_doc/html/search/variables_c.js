var searchData=
[
  ['matplotlibversion_505',['matplotlibVERSION',['../namespaceRuntime__check.html#a2219d4757a1451b5fcf25fc05ec91fe2',1,'Runtime_check']]],
  ['momdata_506',['momData',['../namespaceError__from__formula.html#ab8734e3661c151e07257feb2cc853a4e',1,'Error_from_formula.momData()'],['../namespaceRegression__Loop__Noise.html#aa33f66246115e0a6a023afa597f0b0b0',1,'Regression_Loop_Noise.momData()'],['../namespaceRegression__Loop__NShots.html#ac0837ae9fd88b49306765ab29c2e64e2',1,'Regression_Loop_NShots.momData()']]],
  ['momlabels_507',['momLabels',['../namespaceError__from__formula.html#ad9c2437edd310523113f2c526164c24d',1,'Error_from_formula.momLabels()'],['../namespaceRegression__Loop__Noise.html#ab71461058013a262519472d35e1a1c0d',1,'Regression_Loop_Noise.momLabels()'],['../namespaceRegression__Loop__NShots.html#af36af97cd494c885305f4ee2782562be',1,'Regression_Loop_NShots.momLabels()']]]
];

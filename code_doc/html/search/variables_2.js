var searchData=
[
  ['background_435',['background',['../namespaceHyperParameterOpt.html#a040f4bfc39f3bb8eede7d343be1e35df',1,'HyperParameterOpt']]],
  ['batch_5fsize_436',['batch_size',['../classDataGenerator_1_1DataGenerator.html#ab296e5b081f8b93e0df2a93f5b8baae1',1,'DataGenerator::DataGenerator']]],
  ['bbox_5finches_437',['bbox_inches',['../namespaceVisualization.html#a16b88141a5fcdd068c119068bd4c2551',1,'Visualization.bbox_inches()'],['../namespaceVisualizationNew.html#ae8239870dfdaf5c6cb5ff64479b3be1e',1,'VisualizationNew.bbox_inches()']]],
  ['best_5fval_5floss_5findex_438',['best_val_loss_index',['../namespaceRegression__Loop__Noise.html#a1f6682e5884765371109cad1e9cd5fd8',1,'Regression_Loop_Noise.best_val_loss_index()'],['../namespaceRegression__Loop__NShots.html#a95ff3c7b93165c17e122fcc16c8c0a9b',1,'Regression_Loop_NShots.best_val_loss_index()']]],
  ['bohb_439',['bohb',['../namespaceHyperParameterOpt.html#a146ad54a125481d12dee8cd09eb3792b',1,'HyperParameterOpt']]],
  ['bottom_440',['bottom',['../namespaceVisualization.html#a7615539e77c6d21800dab236441df04c',1,'Visualization.bottom()'],['../namespaceVisualizationNew.html#a43f2fb4751b62208ff5f4f4a1b188ee9',1,'VisualizationNew.bottom()']]],
  ['boxline_441',['boxLine',['../namespaceOutput.html#a49e4cdc8b05567b1925e5a0f9195327c',1,'Output']]]
];

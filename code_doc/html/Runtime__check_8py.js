var Runtime__check_8py =
[
    [ "RemoveAfterLastSlash", "Runtime__check_8py.html#aacff24e8f2e98e08b6f569f5989777e0", null ],
    [ "runtime_checks", "Runtime__check_8py.html#a3e0d4aac87e7ff2cdcc16c694c17548f", null ],
    [ "DataDir", "Runtime__check_8py.html#a023f4c86825658cecdd62b77fc49ec38", null ],
    [ "end", "Runtime__check_8py.html#a3c0bc2b94d088c0c9a42f13663b7d10c", null ],
    [ "exist_ok", "Runtime__check_8py.html#a0fca99e754f371674c7de3a592d5ec15", null ],
    [ "IDs_cross", "Runtime__check_8py.html#ac1f0862ee198d3fef2e82fcaca7d23cb", null ],
    [ "IDs_tot", "Runtime__check_8py.html#ae01a818e6fdae4502a9858a14d0350d0", null ],
    [ "matplotlibVERSION", "Runtime__check_8py.html#a2219d4757a1451b5fcf25fc05ec91fe2", null ],
    [ "NTestDatasets", "Runtime__check_8py.html#a6cc5811129a8b4590154a61bbab8667f", null ],
    [ "NTrainDatasets", "Runtime__check_8py.html#a5aa172f97066c17be05d02ed40f49024", null ],
    [ "numpyVERSION", "Runtime__check_8py.html#ac24f4d4ee72b64db7f81bbfc686da91c", null ],
    [ "NValidationDatasets", "Runtime__check_8py.html#abea97e513202de85225a08bdd5103703", null ],
    [ "pythonVERSION", "Runtime__check_8py.html#a6d145fdc5d70b3af8b6ef8e1c53cd711", null ],
    [ "scipyVERSION", "Runtime__check_8py.html#a22d74b292cd6fbd7d336b17db527569a", null ],
    [ "sectionLine", "Runtime__check_8py.html#af392ffcb35eed34bbf337509ec0aba7c", null ],
    [ "tf", "Runtime__check_8py.html#a33fb39caa3b212d9d1921da3e9fcdd3f", null ],
    [ "tfVERSION", "Runtime__check_8py.html#a8fc57ab646214acbf02bfab8f6dc2994", null ]
];
\hypertarget{index_autotoc_md0}{}\doxysection{U\+N\+I\+Q\+O\+R\+N – Universal Neural Network Interface for Quantum Observable Readout from N-\/body wavefunctions}\label{index_autotoc_md0}
--- V\+E\+R\+S\+I\+ON 0.\+7 B\+E\+TA ---\hypertarget{index_autotoc_md1}{}\doxysubsubsection{Introduction}\label{index_autotoc_md1}
The purpose of \mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}} is to implement the inference of observables from measurements of quantum states as a machine learning task, see \href{https://arxiv.org/abs/2010.14510}{\texttt{ this preprint}} for a description. \mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}} is a repository of python and bash scripts that implement classification or regression tasks using the \href{https://www.tensorflow.org}{\texttt{ Tensor\+Flow}} library. The code performs the training of artificial neural networks on data for various observables obtained from \href{http://ultracold.org}{\texttt{ M\+C\+T\+D\+H-\/X}} simulations.

Currently, only single-\/shot images as input data are supported, but in the future using correlation functions as input will be implemented.

The quantities that can be analyzed so far are fragmentation, particle number, density distributions, the potential, one-\/ and two-\/body density matrices, and correlation functions.

Minimal single-\/shot datasets are included in the repository for testing purposes. For use beyond testing, please download the complete \href{https://drive.google.com/file/d/1y1cZWaAo57e4CVL_jv9Q6T3E6Q4XHh-I/view?usp=sharing}{\texttt{ double-\/well}} and \href{https://drive.google.com/file/d/1Zqc8wyzeqWrna-7uMJ9XI_WFreQBJzDu/view?usp=sharing}{\texttt{ triple-\/well}} dataset.

The folder, where the data is read from is configured -- together with other properties of the task to perform -- in the {\bfseries{Input.\+py}} file. The {\itshape double-\/well} dataset contains 3000 and the {\itshape triple-\/well} dataset contains 3840 random ground states (randomized parameters\+: barrier heights and widths, interparticle interactions, particle number).\hypertarget{index_autotoc_md2}{}\doxysubsubsection{Prerequisites}\label{index_autotoc_md2}
As prerequisites to run the python scripts in this repository you will need (at least)


\begin{DoxyItemize}
\item Tensorflow 2 (for building and training ML models)
\item Tensorflow-\/\+Addons (for using interpolation loss)
\item Matplotlib (for visualizing the results)
\item Numpy (for numerical manipulations)
\item Jupyter (optional, for executing the notebook)
\end{DoxyItemize}\hypertarget{index_autotoc_md3}{}\doxysubsubsection{Structure of the repository}\label{index_autotoc_md3}
Please refer to the flowchart \char`\"{}workflow.\+pdf\char`\"{} for a graphical depiction of the structure of the modularized code. The \mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}} python modules related to machine learning tasks are stored mostly in the \char`\"{}source\char`\"{} directory and the python modules and files related to the data generation with M\+C\+T\+D\+H-\/X are stored in the directory \char`\"{}\+M\+C\+T\+D\+H\+X-\/data\+\_\+generation\char`\"{}.

Calculations can be done using the Jupyter notebook {\bfseries{\mbox{\hyperlink{UNIQORN_8ipynb}{U\+N\+I\+Q\+O\+R\+N.\+ipynb}}}} or the python script {\bfseries{\mbox{\hyperlink{UNIQORN_8py}{U\+N\+I\+Q\+O\+R\+N.\+py}}}}. An evaluation of the error of some observables is possible via a formula. For this purpose, the python script {\bfseries{\mbox{\hyperlink{Error__from__formula_8py}{Error\+\_\+from\+\_\+formula.\+py}}}} can be executed. To perform the (lengthy!) check for the dependence of the neural-\/network-\/based regression of observables from input data with a varying amount of single-\/shot observations per input dataset, the python script {\bfseries{\mbox{\hyperlink{Regression__Loop__NShots_8py}{Regression\+\_\+\+Loop\+\_\+\+N\+Shots.\+py}}}} can be executed.

\mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}}\textquotesingle{}s directories contain other python modules that have different purposes.

P\+Y\+T\+H\+ON M\+O\+D\+U\+L\+ES\+:
\begin{DoxyItemize}
\item Input.\+py\+: This python class contains all the parameters of the algorithm, ranging from which observable to reconstruct, to how to batch the input data, to what to visualize or not, etc.
\item \mbox{\hyperlink{Output_8py}{Output.\+py}}\+: This module contains all necesarry functionalities to generate the \mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}} run summary and support a more cohesive structured storage of results including Tensor\+Board logs and Vizualization.\+py plots. This is achieved through run-\/wide consistent, centrally stored time stamps collected in a directory called \char`\"{}./output\char`\"{} .
\item \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}}\+: This module performs runtime checks for input data consistency, and extracts and manipulates the input data via the subroutine \mbox{\hyperlink{DataLoading_8py}{Data\+Loading.\+py}} depending on the given task (i.\+e. supervised or unsupervised regression/classification) and on the given observable to fit (e.\+g. fragmentation, or density, or...). It also splits the input data into training, validation, and test sets.
\item \mbox{\hyperlink{DataLoading_8py}{Data\+Loading.\+py}}\+: This module imports the input data (single shots or correlation functions) and chooses which observables to use as labels (classification) or true values (regression).
\item \mbox{\hyperlink{DataGenerator_8py}{Data\+Generator.\+py}}\+: This module implements a class to dynamically load the data on-\/demand, i.\+e., batch-\/by-\/batch while training, validation and testing the model.
\item \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}}\+: This module constructs or loads from a library the type of neural network selected in the input file, compiles it, trains it on the training set, validates it with the validation set, and tests it on a holdout data set.
\item \mbox{\hyperlink{Models_8py}{Models.\+py}}\+: This module is a collection of python functions that return various types of neural networks and other Keras model objects to be used in \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}}. There are default models depending on the task to be performed, an archive of tested models, and customizable models.
\item Model\+Evaluation.\+py\+: This module executes the validation of an already trained model with the test set.
\item \mbox{\hyperlink{Visualization_8py}{Visualization.\+py}}\+: This module generates and saves as .png files all the results of model training and testing (e.\+g. accuracies as a function of the epochs), as well as visual comparisons between the test set and the results obtained with the neural networks.
\item \mbox{\hyperlink{functions_8py}{functions.\+py}}\+: This module contains various useful functions to perform sub-\/tasks.
\item generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py\+: This python script generates random relaxations with M\+C\+T\+D\+H-\/X software to produce the single-\/shot input data.
\end{DoxyItemize}

B\+A\+SH S\+C\+R\+I\+P\+TS (mainly used to generate or import data)\+:
\begin{DoxyItemize}
\item check\+\_\+convergence.\+sh\+: called by generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py . Checks if a random relaxation is converged and restarts it, if not.
\item run\+\_\+anal.\+sh\+: called by generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py . If a random relaxation is converged, this script runs the M\+C\+T\+D\+H-\/X analysis program on it.
\item M\+L\+G.\+sh\+: can be called to parse all python files for a certain string, e.\+g. $\ast$$\ast$ ./\+M\+LG.sh \mbox{\hyperlink{namespaceUNIQORN}{U\+N\+I\+Q\+O\+RN}} $\ast$$\ast$ .
\end{DoxyItemize}

Currently the code supports only supervised regression tasks. The tasks can be implemented via a multilevel perceptron (M\+LP) or a convolutional neural network (C\+NN). Some default and also some customizable models are defined in the file \mbox{\hyperlink{Models_8py}{Models.\+py}}. Note that certain quantities such as fragmentation can only be inferred from multiple (and not a single) single-\/shot images. The \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}} function therefore assembles the input data in stacks of multiple single-\/shot images.\hypertarget{index_autotoc_md4}{}\doxysubsubsection{Quickstart tutorial}\label{index_autotoc_md4}

\begin{DoxyItemize}
\item go to the directory {\bfseries{example\+\_\+run\+\_\+mini}}
\item open Input.\+py and change the \char`\"{}source\+Dir\char`\"{} flag to match the absolute path of your \char`\"{}source\char`\"{} directory and change \char`\"{}\+Dir\+Name\char`\"{} flag to match the absolute path of your data
\item run {\bfseries{python \mbox{\hyperlink{UNIQORN_8py}{U\+N\+I\+Q\+O\+R\+N.\+py}}}}
\item check the results in the subdirectory \char`\"{}output\char`\"{}
\item run summary is in {\bfseries{output/run\+\_\+$<$ date $>$\+\_\+$<$ time $>$.\+out}}
\item change \char`\"{}\+Input.\+py\char`\"{} and rerun \char`\"{}python U\+N\+I\+Q\+O\+R\+N.\+py\char`\"{} to see the effect of your change
\end{DoxyItemize}\hypertarget{index_autotoc_md5}{}\doxysubsubsection{Running the code for a single set of hyperparameters}\label{index_autotoc_md5}
A good start is the Jupyter notebook {\bfseries{\mbox{\hyperlink{UNIQORN_8ipynb}{U\+N\+I\+Q\+O\+R\+N.\+ipynb}}}}, which calls all the different modules that perform various tasks.

You can check it out by typing

{\ttfamily jupyter notebook}

in your shell. This should open a window in your web browser, from which you can navigate to the file \mbox{\hyperlink{UNIQORN_8ipynb}{U\+N\+I\+Q\+O\+R\+N.\+ipynb}} and execute it line by line. The notebook goes through the workflow explained above, i.\+e.

data loading -\/$>$ data processing -\/$>$ model choice, training and validation -\/$>$ visualization.

The notebook will automatically call and run other modules such as \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}}, \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}} etc. These files should be modified only if you are a developer implementing new machine learning tasks. Moreover, \mbox{\hyperlink{UNIQORN_8ipynb}{U\+N\+I\+Q\+O\+R\+N.\+ipynb}} is to be seen as a starting point that trains, evaluates and visualizes a model for a single set of model parameters.

To choose which machine learning task to perform (e.\+g. switching from a regression of the particle number from single shots to a classification of fragmented/non-\/fragmented states from correlation functions), you need to modify the input file (python class) {\bfseries{Input.\+py}}. This file contains all the different knobs and variables for the machine learning algorithms, including hyperparameters such as batch size or number of epochs. In here, you can also select which quantity to fit and how, and whether or not you want to load a pre-\/trained neural network or train it yourself, visualize the results or not etc. The input file and the role of each variable therein should be self-\/explanatory.

Note that the notebook produces results in line while being executed, but the corresponding figures are also saved in the various folders in the main folder \char`\"{}plots\char`\"{} for later retrieval. The paths to these files and the files themselves are named after the quantity being fitted. For example, a plot of the accuracy of the regression of the particle number from single shots in real space during 20 epochs will be saved in the folder \char`\"{}plots/\+N\+P\+A\+R/accuracies\char`\"{}, with the name \char`\"{}\+Accuracies-\/for-\/\+R\+E\+G\+R-\/of-\/\+N\+P\+A\+R-\/from-\/\+S\+S\+S-\/in-\/x-\/space-\/during-\/20-\/epochs.\+png\char`\"{}\hypertarget{index_autotoc_md6}{}\doxysubsubsection{Performing a hyperparameter optimization with H\+P\+Bandster}\label{index_autotoc_md6}
It is a tough task to optimally configure all the possible parameters of deep learning models. However, since hyperparameter optimization is an optimization task, it can be automated. One library that provides out-\/of-\/the-\/box hyperparameter optimization is the Hp\+Band\+Ster library. See \href{https://github.com/automl/HpBandSter}{\texttt{ this link}} for details about Hp\+Band\+Ster. Obviously, the Hp\+Band\+Ster library is a prerequisite for running the code.

Currently, we only provide an Hp\+Band\+Ster implementation for optimizing convolutional neural networks (Set Model=\textquotesingle{}custom\textquotesingle{} and Conv\+Net=True in Input.\+py). You can run the hyperparameter optimization by executing


\begin{DoxyItemize}
\item {\ttfamily python \mbox{\hyperlink{HyperParameterOpt_8py}{Hyper\+Parameter\+Opt.\+py}}}
\end{DoxyItemize}

This will then perform an optimization which you can visualize by running


\begin{DoxyItemize}
\item {\ttfamily python \mbox{\hyperlink{PlotInteractiveHyperParameterOpt_8py}{Plot\+Interactive\+Hyper\+Parameter\+Opt.\+py}}}
\end{DoxyItemize}

This will open plot the results of the optimization run. By clicking on the point in the plot with the lowest loss, you can find out the optimal set of hyperparameters, i.\+e., the result of the optimization. 
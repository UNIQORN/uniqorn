import numpy
import time

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

from hpbandster.core.worker import Worker

import DataPreprocessing
import ModelTrainingAndValidation
import ModelEvaluation
import Models
import Visualization
import Input as inp
import DataLoading
import DataGenerator

class MyWorker(Worker):

    def __init__(self, *args, sleep_interval=0, **kwargs):
        super().__init__(*args, **kwargs)

        #get data with the data preprocessing module:

        DataLoading.runtime_checks()
        IDs_training, IDs_test = DataLoading.data_ID_loader(data=inp.Training_Data)
        if inp.batch_loading==True:
           self.training_generator, self.validation_generator, self.y_val = DataGenerator.InitializeDataGenerators()
        elif inp.batch_loading==False:
           self.training_generator, self.validation_generator, self.X_train, self.y_train, self.X_val, self.y_val = DataGenerator.InitializeDataGenerators()

        self.y_val=numpy.array(self.y_val)
        print('Shape of y_val:'+str(numpy.shape(self.y_val)))
        ##################################################################################################################



        print('\n')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('Successfully loaded training and test sets.')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('\n')
  
        self.sleep_interval = sleep_interval

    def compute(self, config, budget, **kwargs):
        """
           hyperparameter optimization for convolutional networks
        """
  
        print("building CNN with " + str(config['num_conv_layers']) + " layers---")
        inp.filters=[]
        inp.pooling=[]
        inp.kernelsizes=[]

        for i in range(config['num_conv_layers']):
           filtername="num_filters_"+str(i+1)
           inp.filters.append(config[filtername]) 
           filtername="pooling_"+str(i+1)
           inp.pooling.append(config[filtername]) 
           filtername="kernel_size_"+str(i+1)
           inp.kernelsizes.append((config[filtername],config[filtername])) 
  
        print("assembled filters:::"+str(inp.filters))
        print("assembled pooling:::"+str(inp.pooling))
        print("assembled kernelsizes:::"+str(inp.kernelsizes))

        if inp.batch_loading==True:
            (_, 
            history, 
            _,
            _) = ModelTrainingAndValidation.main(self.training_generator, 
                                                 self.validation_generator, 
                                                 self.y_val)
            
        elif inp.batch_loading==False:
            (_, 
            history, 
            _,
            _) = ModelTrainingAndValidation.main(self.X_train,
                                                 self.y_train, 
                                                 self.X_val, 
                                                 self.y_val)
                                                                                                                                                                                 


        res = history.history['val_loss'][-1]  
        print ("loss:::"+str(float(res)))
        Models.clear()
        time.sleep(self.sleep_interval)

        return({
                    'loss': float(res),  # this is the a mandatory field to run hyperband
                    'info': res  # can be used for any user-defined information - also mandatory
                })

    @staticmethod
    def get_configspace():

            """
            It builds the configuration space with the needed hyperparameters.
            It is easily possible to implement different types of hyperparameters.
            Beside float-hyperparameters on a log scale, it is also able to handle categorical input parameter.
            :return: ConfigurationsSpace-Object
            """
            cs = CS.ConfigurationSpace()


            num_conv_layers =  CSH.UniformIntegerHyperparameter('num_conv_layers', lower=1, upper=4, default_value=3)


            num_filters_1=CSH.UniformIntegerHyperparameter('num_filters_1', lower=4, upper=64, default_value=16, log=True)
            num_filters_2=CSH.UniformIntegerHyperparameter('num_filters_2', lower=4, upper=64, default_value=16, log=True)
            num_filters_3=CSH.UniformIntegerHyperparameter('num_filters_3', lower=4, upper=64, default_value=16, log=True)
            num_filters_4=CSH.UniformIntegerHyperparameter('num_filters_4', lower=4, upper=64, default_value=16, log=True)
            num_filters_5=CSH.UniformIntegerHyperparameter('num_filters_5', lower=4, upper=64, default_value=16, log=True)
            num_filters_6=CSH.UniformIntegerHyperparameter('num_filters_6', lower=4, upper=64, default_value=16, log=True)
            num_filters_7=CSH.UniformIntegerHyperparameter('num_filters_7', lower=4, upper=64, default_value=16, log=True)
            num_filters_8=CSH.UniformIntegerHyperparameter('num_filters_8', lower=4, upper=64, default_value=16, log=True)


            pooling_1 = CSH.CategoricalHyperparameter('pooling_1', choices=['max', 'avg','none'])
            pooling_2 = CSH.CategoricalHyperparameter('pooling_2', choices=['max', 'avg','none'])
            pooling_3 = CSH.CategoricalHyperparameter('pooling_3', choices=['max', 'avg','none'])
            pooling_4 = CSH.CategoricalHyperparameter('pooling_4', choices=['max', 'avg','none'])
            pooling_5 = CSH.CategoricalHyperparameter('pooling_5', choices=['max', 'avg','none'])
            pooling_6 = CSH.CategoricalHyperparameter('pooling_6', choices=['max', 'avg','none'])
            pooling_7 = CSH.CategoricalHyperparameter('pooling_7', choices=['max', 'avg','none'])
            pooling_8 = CSH.CategoricalHyperparameter('pooling_8', choices=['max', 'avg','none'])


            kernel_size_1=CSH.UniformIntegerHyperparameter('kernel_size_1', lower=2, upper=16, default_value=2, log=True)
            kernel_size_2=CSH.UniformIntegerHyperparameter('kernel_size_2', lower=2, upper=16, default_value=2, log=True)
            kernel_size_3=CSH.UniformIntegerHyperparameter('kernel_size_3', lower=2, upper=16, default_value=2, log=True)
            kernel_size_4=CSH.UniformIntegerHyperparameter('kernel_size_4', lower=2, upper=16, default_value=2, log=True)
            kernel_size_5=CSH.UniformIntegerHyperparameter('kernel_size_5', lower=2, upper=16, default_value=2, log=True)
            kernel_size_6=CSH.UniformIntegerHyperparameter('kernel_size_6', lower=2, upper=16, default_value=2, log=True)
            kernel_size_7=CSH.UniformIntegerHyperparameter('kernel_size_7', lower=2, upper=16, default_value=2, log=True)
            kernel_size_8=CSH.UniformIntegerHyperparameter('kernel_size_8', lower=2, upper=16, default_value=2, log=True)


            cs.add_hyperparameters([num_conv_layers, 
                                    num_filters_1, 
                                    num_filters_2, 
                                    num_filters_3, 
                                    num_filters_4, 
                                    num_filters_5, 
                                    num_filters_6, 
                                    num_filters_7,
                                    num_filters_8,
                                    pooling_1,
                                    pooling_2, 
                                    pooling_3, 
                                    pooling_4,
                                    pooling_5,
                                    pooling_6, 
                                    pooling_7, 
                                    pooling_8,
                                    kernel_size_1,
                                    kernel_size_2,
                                    kernel_size_3,
                                    kernel_size_4,
                                    kernel_size_5,
                                    kernel_size_6,
                                    kernel_size_7,
                                    kernel_size_8])


            return cs

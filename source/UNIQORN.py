#> Synopsis: this Python script provides a working 
#>           example for a machine learning task with
#>           MCTDH-X data. 

######################################################################
#########       VERBOSITY OF TENSORFLOW LOGGING              #########
######################################################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

# The verbosity levels are:
# --------------------------
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed



######################################################################
#########       IMPORT SOURCE DIRECTORY PATH                 #########
######################################################################

import Input as inp


global pathToSource # make sure to change the global variable
if (hasattr(inp,'SourceDir')):
    pathToSource = inp.SourceDir
else:
    print('SourceDir not set in Input.py: trying ../source/')
    pathToSource = '../source/'
    inp.SourceDir = '../source/'

import sys
sys.path.insert(1, pathToSource)

import Output as out

#########################################################

###########################################################################
#######      CHECK MODULE AVAILABITLITY AND INPUT PARAMETERS        #######
###########################################################################

import Runtime_check
Runtime_check.runtime_checks()
del Runtime_check

#########################################################

#########################################################
#########                 IMPORTS               #########
#########################################################
import numpy as np
from tensorflow.keras.utils import Sequence
from importlib import reload
from datetime import datetime

# import UNIQORN modules and classes
import DataLoading
import DataPreprocessing
import DataGenerator
import ModelTrainingAndValidation
import Visualization
import Output as out
import Input as inp
#########################################################

#########################################################
######### TIMESTAMP: START THE CLOCK, LET'S GO :) #######
#########################################################

out.startTime = datetime.now()

# MAKE OUTPUT DIRECTORY AND CHANGE INTO IT
if os.path.isdir('output') != True:
    os.mkdir('output')
os.chdir('output')

#########################################################

###########################################################################
#######      LOADING FILENAMES AND INSTANTIATING DATA GENERATORS    #######
###########################################################################
# Instantiation of the data generators for batch processing
# with the paths to the files used as data.
# This construction is needed to load the data and labels on the fly,
# to not overload the memory for very large data sets.
if inp.batch_loading==True:
    training_generator, validation_generator, test_generator, y_test = DataGenerator.InitializeDataGenerators()
elif inp.batch_loading==False:
    training_generator, validation_generator, test_generator, X_train, y_train, X_val, y_val, X_test, y_test = DataGenerator.InitializeDataGenerators()

y_test=np.array(y_test)

#########################################################

#########################################################
########     TIMESTAMP: PRE TRAINING             ########
#########################################################

out.postLoadTime = datetime.now()
out.durationDataLoading = out.calcDuration('Data Loading')
out.writeDataSummary()

#########################################################

##########################################################
########                   MAIN                 ##########
##########################################################
#train (or load already trained) model and validate it  
if inp.TrainingFlag==True:   
    # Tweak input parameters to optimize performance of custom NNs
    ##########################################################
    #inp.layers=[1024,512,64]
    #inp.regularizations=[0.4,0.2,0.6]
    # ....
    if inp.batch_loading==True:
        (model, 
        history, 
        test_predictions,y_test, score) = ModelTrainingAndValidation.main(training_generator, 
                                                                   validation_generator, 
                                                                   test_generator, 
                                                                   y_test)
        
    elif inp.batch_loading==False:
        (model, 
        history, 
        test_predictions, y_test, score) = ModelTrainingAndValidation.main(X_train,  
                                                                   y_train, 
                                                                   X_val, 
                                                                   y_val,
                                                                   X_test,
                                                                   y_test)
                                                                                                                                                                                                                                                                  
elif inp.TrainingFlag==False:                                                                                                                                                                                                                                 
    print('No neural network training requested. Please load the already trained neural network then: ')                                                                                                                                                          
    print('STILL NEEDS TO BE IMPLEMENTED IN DataTraining.py / Models.py!')                                                                                                                                                                                        
                                                                                                                                                                                                                                                                  
    #train (or load already trained) data with DataTraining module:                                                                                                                                                                                               
    (model, 
    test_predictions, y_test, score) =  ModelTrainingAndValidation.main()

#################################################################### 

##########################################################
########      TIMESTAMP: post Training          ##########
##########################################################
out.postTrainTime = datetime.now()
out.durationTraining = out.calcDuration('Training and Evaluation')
out.writeModelSummary(model)
out.writeEvalSummary(score)
##########################################################

##########################################################
########             VISUALIZATION              ##########
##########################################################
if inp.VisualizationFlag==True:                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                 
   print('Accessing Visualization module...')                                                                                                                                                                                                                
   Visualization.main(y_test, history, test_predictions, inp.NViz)                                                                                                                                                          
                                                                                                                                                                                                                                                                  
elif inp.VisualizationFlag==False:                                                                                                                                                                                                                            
   print('No visualization of the results requested!')                                                                                                                                                                                                      
   print('\n')                                                                                                                                                                                                                                              
   print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')                                                                                                                                                                                                                    
   print('Closing up now. Bye, Felicia.')                                                                                                                                                                                                                   
   print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')                                                                                                                                                                                                                    
   print('\n')   

##########################################################
########      TIMESTAMP: final                  ##########
##########################################################
out.endTime = datetime.now()
out.durationVisualization = out.calcDuration('Visualization')
out.durationTotal = out.calcDuration('Total')
out.finalize()
                                                                                                                                                                                                                                                                
##########################################################

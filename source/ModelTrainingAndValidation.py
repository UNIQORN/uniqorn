# this routine compiles, fits, and evaluates Keras ML models 
# INPUT: training data, training labels, test data, test labels
# OUTPUTS: model object, history object, 
#          and test predictions of the application of the model on the test data 

#########################################################
#########                 IMPORTS               #########
#########################################################
#portability imports:
from __future__ import print_function

#predefined packages:
import numpy as np
from scipy import interpolate
import sys
import os
import matplotlib.pyplot as plt # for plotting history etc
import multiprocessing #for using multicore cpus
import pydot
from datetime import datetime

#machine learning packages 
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import ReduceLROnPlateau,EarlyStopping,TensorBoard,Callback
from tensorflow.keras.optimizers import Adam
import tensorflow as tf
##################################################################################################################

#custom-made modules and classes:
import Input as inp  # import input file
import Output as out
import Models
##################################################################################################################

#some options
np.set_printoptions(threshold=sys.maxsize)

xgrid=np.arange(0.0,1.0*inp.Npoints)
ygrid=np.arange(0.0,1.0*inp.Npoints)


# function to create a TF variable from a tuple
def make_variable(tuple, initializer):
    return tf.Variable(initializer(shape=tuple, dtype=tf.float32))

# return shape of a TF tensor as an iterable tuple
def shape(tensor):
    s = tensor.get_shape()
    return tuple([s[i].value for i in range(0, len(s))])

# function for plot an interpolation (for testing TFs interpolate_spline)
def plot_interpol(xesnew,ynew):
    fig, ax = plt.subplots()
    ax.plot(xesnew, ynew,label='interpolation', marker='+')
   
    ax.set(xlabel='point', ylabel='value')
    ax.grid()
    ax.legend()
   
    fig.savefig("test_interpolate_spline.png")
    plt.show()

# Apply the learned interpolation to the output to generate output for visualization
def ApplyInterpolation(y_data,y_data_interpolation):

    xnew=tf.linspace(np.float32(-5),np.float32(5),inp.Npoints)
    xnew=tf.expand_dims(xnew,0)
    xnew=tf.expand_dims(xnew,2)

    if (inp.LearnInterpolationKnots==True):
        x,z=tf.split(y_data,2,axis=1)
        print(out.formatOutput('knots of interpolation',x), end='')
        x=tf.expand_dims(x,2)
        z=tf.expand_dims(z,2)
    else:   
        x=tf.linspace(np.float32(-5),np.float32(5),inp.InterpolNpoints)
        x=tf.expand_dims(x,0)
        x=tf.expand_dims(x,2)
        z=y_data
        z=tf.expand_dims(z,1)
 

    y_data_interpolation=interpolate_spline(x, z, xnew, order=inp.InterpolOrder,regularization_weight=inp.RegularizationWeight)
    y_data_interpolation=tf.squeeze(y_data_interpolation,axis=2)
    return y_data_interpolation

##################################################################################################################
# Custom Loss function evaluating the error of an interpolation
##################################################################################################################
def Interpolation_MSE(y_true,y_pred):
    
    from tensorflow_addons.image import interpolate_spline        

    if (inp.Learn=='DENS' or inp.Learn=='POT'):
        xnew=tf.linspace(np.float32(-5),np.float32(5),inp.Npoints)
        xnew=tf.expand_dims(xnew,0)
        xnew=tf.expand_dims(xnew,2)

        if (inp.LearnInterpolationKnots==True):
            x,z=tf.split(y_pred,2,axis=1)
            print(out.formatOutput("type of x",type(x)), end='')
            x=tf.expand_dims(x,2)
            z=tf.expand_dims(z,2)
            znew=interpolate_spline(x, z, xnew, order=inp.InterpolOrder,regularization_weight=inp.RegularizationWeight)
            znew=tf.squeeze(znew,axis=2)
        else:   
            x=tf.linspace(np.float32(-5),np.float32(5),inp.InterpolNpoints)
            x=tf.expand_dims(x,0)
            x=tf.expand_dims(x,2)
            z=tf.expand_dims(y_pred,2)
            znew=interpolate_spline(x, z, xnew, order=inp.InterpolOrder,regularization_weight=inp.RegularizationWeight)

        print(out.formatOutput('shape of x',x.get_shape().as_list()), end='')
        print(out.formatOutput('shape of xnew',xnew.get_shape().as_list()), end='')
#       print(out.formatOutput('shape of z,z.get_shape().as_list()), end='')
        print(out.formatOutput('shape of znew',znew.get_shape().as_list()), end='')

        loss = K.square(znew - y_true)
        loss = K.mean(loss, axis=1)
        loss = K.sqrt(loss)


    elif (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO2' or inp.Learn=='RHO1'):
        x=y_prednp[0:inp.InterpolNpoints-1]
        y=y_prednp[inp.InterpolNpoints:2*inp.InterpolNpoints-1]
        z=y_prednp[2*inp.InterpolNpoints:]
        interpolation=interpolate.interp2d(x, y, z, kind='cubic')
        znew=interpolation(xgrid,ygrid)    

    elif (inp.Learn=='PHASEHIST'):
        print("Cannot generate interpolations for Histograms yet!")
        exit()
   
#    tf.print(loss, output_stream=sys.stderr)
    return loss

# loss function that adds MSE and MAE 
def MSE_plus_MAE(y_true,y_pred):
    loss=200*tf.keras.losses.mean_absolute_error(y_true, y_pred)+tf.keras.losses.mean_squared_error(y_true, y_pred)
    return loss

# loss function that adds MSE and log_MSE
def MSE_plus_LogMSE(y_true,y_pred):
    loss=tf.keras.losses.mean_squared_logarithmic_error(y_true, y_pred)+tf.keras.losses.mean_squared_error(y_true, y_pred)
    return loss

# loss function that adds logCosh and MAE
def LogCosh_plus_MAE(y_true,y_pred):
    l=tf.keras.losses.LogCosh(reduction=tf.keras.losses.Reduction.NONE)
    loss=tf.keras.losses.mean_absolute_error(y_true, y_pred)+l(y_true, y_pred)
    return loss

# loss function that adds logCosh and logMSE
def LogCosh_plus_LogMSE(y_true,y_pred):
    l=tf.keras.losses.LogCosh(reduction=tf.keras.losses.Reduction.NONE)
    loss=25.0*tf.keras.losses.mean_squared_logarithmic_error(y_true, y_pred)+l(y_true,y_pred)
    return loss

# loss function that adds logCosh and logMSE
def LogCosh_plus_MSE(y_true,y_pred):
    l=tf.keras.losses.LogCosh(reduction=tf.keras.losses.Reduction.NONE)
    loss=tf.keras.losses.mean_squared_error(y_true, y_pred)+150.0*l(y_true,y_pred)
    return loss

# loss function that adds logCosh and logMSE and MAE
def LogCosh_plus_LogMSE_plus_MAE(y_true,y_pred):
    l=tf.keras.losses.LogCosh(reduction=tf.keras.losses.Reduction.NONE)
    loss=tf.keras.losses.mean_squared_logarithmic_error(y_true, y_pred)+l(y_true,y_pred)+tf.keras.losses.mean_absolute_error(y_true, y_pred)
    return loss

class CustomCallback(Callback):
    def on_train_batch_begin(self, batch, logs=None):
        keys = list(logs.keys())
        print("... batch_size:"+str(batch.shape))
        print("...Training: start of batch {}; got log keys: {}".format(batch, keys))


# main function for creating Keras models
def main(*args,**kwargs):
    
    if inp.batch_loading==True:
        training_generator=args[0]
        validation_generator=args[1]
        test_generator=args[2]
        y_test=args[3]
    elif inp.batch_loading==False:
        X_train=args[0]
        y_train=args[1]
        X_val=args[2]
        y_val=args[3]
        X_test=args[4]
        y_test=args[5]

    if inp.TrainingFlag==True:
    
        #Choose which type of model to use:
        if inp.Model=='default':
            model = Models.default()
            
        elif inp.Model=='custom':
            model = Models.custom(ConvNet=inp.ConvNet)
        
        elif inp.Model=='archive':
            Model_Name=inp.Model_Name
            model = Models.archive(Model_Name)


        ####################################
        ###  SETUP FOR TRAINING OF MODEL ###
        ####################################

        # instantiate Adam with default parameters
        adamOpti = Adam()

        # create loss objects as given in the input
        if (inp.InterpolatePredictions==True):
            loss=Interpolation_MSE
            metrics=[Interpolation_MSE]
        else:
            if inp.Loss=='mse':
               metrics=['mse','mae']
               loss='mean_squared_error' 
            elif inp.Loss=='mae':
               metrics=['mse','mae']
               loss='mean_absolute_error' 
            elif inp.Loss=='log_mse':
               loss=tf.keras.losses.MeanSquaredLogarithmicError()
               metrics=[tf.keras.metrics.MeanSquaredLogarithmicError(),'mae','mse']
            elif inp.Loss=='log_cosh':
               loss=tf.keras.losses.LogCosh()
               metrics=[tf.keras.metrics.LogCoshError(),'mae','mse']
            elif inp.Loss=='mse_logmse':
               loss=MSE_plus_LogMSE
               metrics=[MSE_plus_LogMSE,'mae','mse']
            elif inp.Loss=='logmse_logcosh':
               loss=LogCosh_plus_LogMSE
               metrics=[LogCosh_plus_LogMSE,tf.keras.metrics.MeanSquaredLogarithmicError(),tf.keras.metrics.LogCoshError(),'mae','mse']
            elif inp.Loss=='mse_logcosh':
               loss=LogCosh_plus_MSE
               metrics=[LogCosh_plus_MSE,tf.keras.metrics.LogCoshError(),'mae','mse']
            elif inp.Loss=='mae_logcosh':
               loss=LogCosh_plus_MAE
               metrics=[LogCosh_plus_MAE,tf.keras.metrics.LogCoshError(),'mae','mse']
            elif inp.Loss=='logmse_logcosh_mae':
               loss=LogCosh_plus_LogMSE_plus_MAE
               metrics=[LogCosh_plus_LogMSE_plus_MAE,tf.keras.metrics.MeanSquaredLogarithmicError(),tf.keras.metrics.LogCoshError(),'mae','mse']
            elif inp.Loss=='mse_mae':
               loss=MSE_plus_MAE
               metrics=[MSE_plus_MAE,'mae','mse']
               
 
        # compile model 
        model.compile(optimizer=adamOpti,
                      loss=loss, 
                      metrics=metrics)
        ##################################################################################################################
        
        
        
        
        ########################################
        ### Print some general information   ###
        ########################################
        print(out.sectionLine, end='')
        print(out.formatSectionTitle('DATA & LOADING'), end='') 
        print('The ANN will learn '+str(inp.Learn)+' from '+str(inp.Training_Data))
        if inp.momData:
            print('Working with MOMENTUM space data.')
        else:
            print('Working with POSITION space data.')
        if inp.momLabels:
            print('Working with MOMENTUM space labels.')
        else:
            print('Working with POSITION space labels.')
        if inp.AxisFlipping:
            print('Data and label flipping along x axis are used for training.')
        if inp.DataThreshold:
            print('Threshold imposed on label: \n    Data with '+str(inp.Learn)+' between ',inp.DataThresholdLow,' and ',inp.DataThresholdHigh,' are discarded.')
            if inp.DataThresholdTest:
                print('    Imposed on both training and testing sets.')
            else:
                print('    Imposed only on training set, but not on testing set.')
        if inp.MultiSSSOrder:
            print('For each wavefunction, ',inp.MultiSSS_NOrder,' different ways of \n\
    single-shot image ordering are fed to the network while training.')
                  
                  
        print(out.sectionLine, end='')
        print(out.formatSectionTitle('MODEL ARCHITECTURE'), end='') 
        model.summary()
        out.PlotModel(model)
        
        # plot model architecture, MIGHT REQUIRE TO INSTALL "pydot"
        #plot_model(model, to_file='plots/{0}/architecture-of-{1}-model-doing-{2}-of-{0}-from-{3}.png'.format(inp.Learn,inp.Model, inp.Job_Type,inp.Training_Data))
        #THIS ONLY HOLDS FOR CNNs FOR NOW:
        #TODO: Make sure that sampling is correct also for MLPs
        NSamples=int(inp.batch_size*inp.NShots/inp.NShotsPerSample)
        NSamplesPerDataset=int(NSamples/inp.batch_size)
        print(out.formatSectionTitle('SAMPLES & BATCHES'), end='')
        print('The total number of datasets (unique wavefunctions) is '+str(inp.NDatasets)+'.')
        print('There are '+str(inp.NShots)+' '+str(inp.Training_Data)+' per dataset.')
        print('Each image is a stack of '+str(inp.NShotsPerSample)+' single shots with '+str(inp.Npoints)+' points.')
        print('The ANN is fed with a total of '+str(NSamples)+' images per batch.')
        print('There are '+str(NSamplesPerDataset)+ ' images per dataset (wavefunction).')
        print(out.sectionLine, end='')
        ##################################################################################################################

                
        #################################
        ###   TRAINING OF THE MODEL   ###
        #################################
        callbacks=[]
        if ('reduce_lr' in inp.callbacks):
            reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.25,
                                          patience=15, min_delta=0.05,
                                          min_lr=0.00000001,verbose=1)
            callbacks.append(reduce_lr)

        if ('earlystopping' in inp.callbacks):
            earlystopping = EarlyStopping(monitor='val_loss', 
                                          patience=30,verbose=1,
                                          restore_best_weights=True)
            callbacks.append(earlystopping)
        
        ##############################################################################
        #       TENSORBOARD CALLBACK
        ##############################################################################
        logdir = out.GetLogDir(out.startTime.strftime("%Y%m%d-%H%M%S"))
        callbacks.append(TensorBoard(log_dir=logdir, histogram_freq=1))#, profile_batch='1,15'))
        ##############################################################################

        ### Uncomment for adding custom callback functions
        #callbacks.append(CustomCallback()) 

        if inp.batch_loading==True:
            #Traing the model with the the data generated on the fly in batches:
            #TODO: extend training to multiprocessing / multiple workers
            history = model.fit(training_generator,
                                          validation_data=validation_generator,
                                          epochs=inp.epochs,
                                          callbacks=callbacks)
                                          #,verbose=1)
                                          #,use_multiprocessing=True,workers=multiprocessing.cpu_count())

        elif inp.batch_loading==False:
            #Traing the model with the fully loaded data sets:
            history = model.fit(X_train,
                                y_train,
                                batch_size=inp.batch_size,
                                epochs=inp.epochs,
                                verbose=1,
                                validation_data=(X_val, y_val),
                                callbacks=callbacks)

        ##################################################################################################################
        # Save model
        fname=out.GetModelName(out.startTime.strftime("%Y%m%d-%H%M%S"))
        model.save(fname)
        ##################################################################################################################

        print('\n')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('Neural network successfully trained with training set.')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('\n')                                                                                                                                                                                                                                                   
                                          
    elif inp.TrainingFlag==False:
        print('\n')                                                                                                                                                                                                                                                   
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('Loading already trained neural networks from files not yet implemented!')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('\n')
        #TODO: something like: model, history test_predictions = something.load(inp.TrainedNeuralNetwork.format(BLABLA))? Check syntax etc.
        #return model, history

##################################################################################################################
    #test data:                                                                                                                                                                                                                                                   
    ##############################
    ###   OBTAIN PREDICTIONS   ###
    ##############################################################################################################
    if inp.batch_loading==True:
        test_predictions, score = ModelEvaluation(model, test_generator)
        print('\n')
    elif inp.batch_loading==False:
        test_predictions, score = ModelEvaluation(model, X_test, y_test)

    
    # Consistency check to see if dimensions of labels and predictions match:
    if (str(np.shape(test_predictions)) != str(np.shape(y_test))):
        size_predictions = 1
        for dim in np.shape(test_predictions):
            size_predictions *= dim
        size_labels = 1
        for dim in np.shape(y_test):
            size_labels *= dim
        if size_labels != size_predictions:
            print('Size of lables and predictions do not match up! Please check your data.')
            sys.exit()
        else:
            y_test=y_test.reshape(np.shape(test_predictions))
    
    print('Consistency check to see if dimensions of labels and predictions match:')
    print(out.formatOutput("SHAPE OF TEST PREDICTIONS",np.shape(test_predictions)), end='')
    print(out.formatOutput("SHAPE OF LABEL",np.shape(y_test)), end='')
    print(out.sectionLine, end='')
    
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('Neural network successfully tested with test set.')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('\n')
    
    if inp.TrainingFlag==True:                      
        return model, history, test_predictions, y_test, score
    elif inp.TrainingFlag==False:
        return model, test_predictions, y_test, score
##################################################################################################################







##################################################################################################################
def ModelEvaluation(*args):
    model=args[0]
    if inp.batch_loading==True:
        test_generator=args[1]
    elif inp.batch_loading==False:
        X_test=args[1]
        y_test=args[2]

    ###################################
    ###   ACCURACY OF THE TRAINING  ###
    ###################################
    #########################################################
    
    # output model performance on the test set  
    # version check because: "return_dict" only available in > TF 2.2.0
    if tf.__version__[2]>= '2':
        if inp.batch_loading==True:
            score = model.evaluate(test_generator, verbose=0, return_dict = True)
        elif inp.batch_loading==False:
            score = model.evaluate(X_test, y_test, verbose=0, return_dict=True)
    else:
        if inp.batch_loading==True:
            score = model.evaluate(test_generator, verbose=0)
        elif inp.batch_loading==False:
            score = model.evaluate(X_test, y_test, verbose=0)
        score_dict = dict()
        for counter, elem in enumerate(score):
            if counter == 0:
                score_dict["loss"] = elem
            else:
                score_dict[model.metrics[counter-1].name] = elem
        score = score_dict

    print(out.sectionLine, end='')
    print(out.formatSectionTitle('Model evaluation'), end='')
    # what is in score?
    print('Metrics:'+ str(model.metrics_names))
    if (inp.InterpolatePredictions==True):
        print(out.formatOutput('Test loss', list(score.values())[0]), end='') 
        print(out.formatOutput('Test InterpolationMSE', list(score.values())[1]), end='')
    else:
        print(out.formatOutput('Test loss', list(score.values())[0]), end='')
        print(out.formatOutput('Test MSE', list(score.values())[1]), end = '')
        print(out.formatOutput('Test MAE', list(score.values())[2]), end = '')
        
    #############################
    ###   TEST OF THE MODEL   ###
    #############################
    if inp.batch_loading==True:
        test_predictions = model.predict(test_generator)
    elif inp.batch_loading==False:
        test_predictions = model.predict(X_test)
    
    if (inp.InterpolatePredictions==True):
        print("In ModelEvaluation- shape of test_predictions:", str(test_predictions.shape))
        
        test_predictions=ApplyInterpolation(test_predictions,test_predictions)
        print("In ModelEvaluation- after interpolation shape of test_predictions:", str(test_predictions.shape))

    print(out.sectionLine, end='')
    test_predictions = np.array(test_predictions)   #make sure predictions are numpy array
    
    ##################################################################################################################
    
    return test_predictions, score
##################################################################################################################

    
if __name__ == "__main__":
    main()

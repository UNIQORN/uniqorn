'''
This module contains all necesarry functionalities to generate the UNIQORN 
run summary and support a more cohesive structured storage of results
including TensorBoard logs and Vizualization.py plots. This is achieved
through run-wide consistent, centrally stored time stamps.

The run summary itself is comprised of three parts: data summary, model
summary and evaluation/results summary.

Beyond that, this output module also provides formatted printing
functions that can be used e.g. to make the shell output's format
consistent with the run summary's format.

TODO: make this a class and all the variables class properties
'''

#-------------------------------------------------------------------
# for file and directory handling, we need sys and os 
#-------------------------------------------------------------------

import sys
import os

#-------------------------------------------------------------------
# begin of actual module: imports
#-------------------------------------------------------------------

from datetime import datetime, timedelta
from contextlib import contextmanager

from matplotlib.pyplot import title
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.activations import relu, selu
from tensorflow.keras.utils import plot_model

import Input as inp


def Set_pathToSource():
    if (hasattr(inp,'SourceDir')):
        path = inp.SourceDir
    else:
        print('SourceDir not set in Input.py: trying ../source/')
        path = '../source/'
    return path

global pathToSource
pathToSource=Set_pathToSource()

global OutputDir
def Set_OutputDir():
    if (hasattr(inp,'OutDir')):
        if not inp.OutDir[-1]=='/':
            OutputDir=inp.OutDir+'/'
        else: 
            OutputDir=inp.OutDir

    else: 
        OutputDir='./output/'
    
    if not (os.path.isdir(OutputDir)):
        os.mkdir(OutputDir)
    return OutputDir
OutputDir=Set_OutputDir()
#-------------------------------------------------------------------
# time variables
#-------------------------------------------------------------------

startTime = 0
postLoadTime = 0
postTrainTime = 0
endTime = 0
timesCalc = False  
durationDataLoading = ''
durationTraining = ''
durationVisualization = ''
durationTotal = ''

def GetModelName(timestamp):
    """
    This function creates the output directory for saving models and returns the name of the save model

    Parameters
    ----------
    timestamp: string with the data/time of the UNIQORN run

    Returns:
    ---------
    ModelName: string with the name assembled for saving the model


    """

    # create directory for models if it does not exist
    ModelDir=OutputDir+'saved_model/'
    if not (os.path.isdir(ModelDir)):
        os.mkdir(ModelDir)

    # assemble the name for saving the model
    if (hasattr(inp,'RunTitle')):
        ModelName=ModelDir+inp.RunTitle+'_'+timestamp+'_model.h5'
    else:
        ModelName=ModelDir+'UNIQORN_'+timestamp+'_model.h5'

    return ModelName


def GetLogDir(timestamp):
    """
    This function creates the output directory for log files of tensorboard and returns the name of this directory

    Parameters
    ----------
    timestamp: string with the data/time of the UNIQORN run

    Returns:
    ---------
    LogDir: string with the created directory

    """

    # directory name of log directory and its creation
    LogDir=OutputDir+'logs/'
    if not (os.path.isdir(LogDir)):
        os.mkdir(LogDir)

    # directory name of subdirectory for the current UNIQORN
    if (hasattr(inp,'RunTitle')):
        LogDir+=inp.RunTitle+'_'+timestamp+'/'
    else: 
        LogDir+='UNIQORN_'+timestamp+'/'

    # create directory for the logs of this run         
    if not (os.path.isdir(LogDir)):
        os.mkdir(LogDir)

    return LogDir


def GetPlotDir(timestamp):
    """
    This function creates the output directory for plots generated from the UNIQORN run

    Parameters
    ----------
    timestamp: string with the data/time of the UNIQORN run

    Returns:
    ---------
    PlotDir: string with the created directory

    """

    # create the root directory if it does not exist
    PlotDir=OutputDir+'plots/'
    if not os.path.isdir(PlotDir):
        os.mkdir(PlotDir)

    # create the name of the sub-directory for this particular run
    if (hasattr(inp,'RunTitle')):
        PlotDirTitle=inp.Learn+'_'+inp.RunTitle+'_'+timestamp
    else: 
        PlotDirTitle=inp.Learn+'_'+'UNIQORN_'+timestamp

    # create the sub-directory for the plots of this particular run      
    PlotDir+=PlotDirTitle
    if not os.path.isdir(PlotDir):
        os.mkdir(PlotDir)

    return PlotDir


def CreateOutputFile():
    """
    This function assembles the first part of the output file name of the UNIQORN summary text file.

    Parameters
    ----------
    none: everything done here depends on parameters in Input.py

    Returns:
    ---------
    Global variable outFilename is modified
    """

    global outFilename
    if (hasattr(inp,'RunTitle')):
        outFilename = OutputDir+'/'+inp.RunTitle 
                
    else:
        outFilename = OutputDir+'/UNIQORN'
        print("Either OutDir or RunTitle definition missing in Input.py -- using default output names")


def PlotModel(model):
    """
    This function visualizes the trained ML model and saves it as a .png file in the output directory
    """
    ModelName=outFilename+"_model.png"
    plot_model(model, ModelName)
    


#-------------------------------------------------------------------
# formatting variables
#-------------------------------------------------------------------

# coherent data presentation
wLeftCol = inp.wLeftCol           # width of the left text columns below
wRightCol = inp.wRightCol          # width of the right text columns below

# for 'tables' at the top and bottom of file
wHeadLeft = inp.wHeadLeft          # width of the left text column in the header
wHeadRight = inp.wHeadRight        # width of the right text column in the header
headLine = '======================================================\n'

# for sectioning: tuneable parameter
wSectionLine = inp.wSectionLine

# for network scheme: tuneable parameter
wLayerBox = inp.wLayerBox          # width of the layer box




############# create lines for sections and headings
sectionLine = ''
headingLine = ''
for i in range(wSectionLine):
    sectionLine += '_'
    headingLine += '-'
sectionLine += '\n'
headingLine += '\n'

############# create lines for ANN visualization
boxLine = ''
for i in range(wLayerBox):
    boxLine += '_'

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def calcDuration(activity):
    """
    Calculates the duration of one of four possible activites (data loading,
    training ans evaluation, visulaization or the total of all activities 
    during the run).

    Parameters
    ----------

    activity: String type, must be one of 4 options specified below

    Returns:
    ---------

    elapsedTime: Sting type, time in the format <hours>:<minutes>:<seconds> 
                or if the run took over a day <days>:<hours>:<minutes>:<seconds> 

    Notes:
    ------

    cuts time off after 11 chars for a more concise output; however, when run 
    takes > 1 day, miliseconds are cut off entirely - not sure what precision 
    is wanted here

    if times weren't explicitely set before, returns 0; maybe a warning should
    be printed out instead
    """

    if activity == 'Data Loading' or activity == 'DataLoading':
        laterTime = postLoadTime
        earlierTime = startTime
    elif activity == 'Training and Evaluation' or activity == 'Training':
        laterTime = postTrainTime
        earlierTime = postLoadTime
    elif activity == 'Visualization':
        laterTime = endTime
        earlierTime = postTrainTime
    elif activity == 'Total':
        laterTime = endTime
        earlierTime = startTime
        
    elapsedTime = laterTime - earlierTime
    # cut off 2 digits abter the second decimal point + convert to int
    return str(elapsedTime)[:11]

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def writeDataSummary():
    """
    Creates an output file or extends an existing one. 
    To be called from a run file after data loading.
    Writes summary of data related information that was
    relevant for the run in question.

    """

    #global outFilename # make sure to change the global variable
    #outFilename = 'run{}.out'.format(startTime.strftime("_%d-%m-%Y_%H-%M-%S"))
    #outFilename += '{}.out'.format(startTime.strftime("_%d-%m-%Y_%H-%M-%S"))
    with open(outFilename,'a+') as f:

	# section _____________________________________________________
        f.write(sectionLine)
	# subsection --------------------------------------------------
        f.write(formatSectionTitle('Data specifications'))


        if inp.Training_Data == 'SSS':
            f.write(formatOutput('Type of features', 'single shot simulations'))
        elif inp.Training_Data == 'DENS':
            f.write(formatOutput('Type of features', 'density distributions')) 
    
        if inp.momData == False:
            f.write(formatOutput('...in space type', 'real'))
        elif inp.momData == True:
            f.write(formatOutput('...in space type', 'momentum'))

        if inp.Learn == 'DENS':       
            f.write(formatOutput('Type of lables', 'density distributions'))
        elif inp.Learn == 'POT':       
            f.write(formatOutput('Type of lables', 'potentials'))

        if inp.momLabels == False:
            f.write(formatOutput('...in space type', 'real'))
        elif inp.momLabels == True:
            f.write(formatOutput('...in space type', 'momentum'))

        f.write('\n')

        f.write(formatOutput('1D/2D/3D', str(inp.Dimensionality)+'D'))
        f.write(formatOutput('grid points',inp.Npoints)) 

        f.write('\n')

        # number of data points for training
        nDataTrain = int(inp.NDatasets * inp.TrainFraction)
        # number of data points for testing
        nDataTest = int(inp.NDatasets - nDataTrain)
        f.write(formatOutput('Number of considered systems',inp.NDatasets)) 
        f.write(formatOutput('...for training:',nDataTrain))
        f.write(formatOutput('...for testing',nDataTest)) 

        f.write('\n')

        f.write(formatOutput('Number of single shots / system',inp.NShots)) 
        f.write(formatOutput('Total number of single shots',int(inp.NShots*inp.NDatasets))) 

        f.write('\n')

        samplesTotal = int(inp.NShots*inp.NDatasets/inp.NShotsPerSample) 
        samplesForTraining = int(samplesTotal*inp.TrainFraction)
        samplesForTesting = int(samplesTotal-samplesTotal*inp.TrainFraction)
        samplesPerSystem =int(inp.NShots/inp.NShotsPerSample) 
        f.write(formatOutput('Number of single shots / sample',inp.NShotsPerSample)) 
        f.write(formatOutput('Number of single shots / system',samplesTotal)) 
        f.write(formatOutput('...for training',samplesForTraining)) 
        f.write(formatOutput('...for testing',samplesForTesting))
        f.write(formatOutput('Number of samples / system',samplesPerSystem)) 
        
        f.write('\n')

        f.write(formatOutput('Number of systems / batch',inp.batch_size)) 
        f.write(formatOutput('Number of samples / batch',int(inp.batch_size*samplesPerSystem))) 
        f.write(formatOutput('Number of epochs',inp.epochs)) 

	# subsection --------------------------------------------------
        f.write(formatSectionTitle('Data preprocessing'))

        f.write(formatOutput('Randomized data',inp.Randomize)) 
        f.write(formatOutput('Shuffled data',inp.Shuffle)) 

	# subsection --------------------------------------------------
        f.write(formatSectionTitle('Training data augmentations'))

        f.write(formatOutput('Data flipped, vertical axis',inp.AxisFlipping)) 
        f.write(formatOutput('MultiSSSOrder',inp.MultiSSSOrder)) 
        f.write(formatOutput('MultiSSS_NOrder',inp.MultiSSS_NOrder)) 
       
	# subsection --------------------------------------------------
        f.write(formatSectionTitle('Data specifications after augmentation'))

        flipMultiplier = 1
        if inp.AxisFlipping == True:
            flipMultiplier = 2
        # number of single shots per wavefunction (=system) in the 
        # augemented trainnig data set:
        augNShots = inp.NShots * flipMultiplier * inp.MultiSSS_NOrder
        f.write(formatOutput('Number single shots / system (training)',augNShots)) 
        f.write(formatOutput('Total number of single shots (training)',int(augNShots*nDataTrain))) 

        f.write('\n')

        f.write(formatOutput('Number of single shots / sample',inp.NShotsPerSample)) 
        f.write(formatOutput('Number of samples (training)',int(nDataTrain*augNShots/inp.NShotsPerSample))) 
        # number of samples per wavefunction (=system) in
        # the augmented training data set:
        trainSamplesPerSystem =int(augNShots/inp.NShotsPerSample) 
        f.write(formatOutput('Number of samples / system (training',trainSamplesPerSystem)) 
        
        f.write('\n')

        f.write(formatOutput('Number of systems / batch',inp.batch_size)) 
        f.write(formatOutput('Number of samples / batch (training)',inp.batch_size*trainSamplesPerSystem)) 

        f.write('\n')
	# end section __________________________________________________
        f.write(sectionLine)




#-------------------------------------------------------------------
#-------------------------------------------------------------------


def writeModelSummary(model):
    """
    Creates an output file or extends an existing one.
    To be called from a run file after model training and validation.
    Writes summary of model related information that was
    relevant for the run in question and creates a layer box based 
    scheme of the architecture.

    """

    # initialize layer counters
    nDenseLayers = 0
    nConvLayers = 0
    nDropoutLayers = 0
    nFlattenLayers = 0

    #global outFilename # make sure to change the global variable
    #outFilename = 'run{}.out'.format(startTime.strftime("_%d-%m-%Y_%H-%M-%S"))
    
    #outFilename += '{}.out'.format(startTime.strftime("_%d-%m-%Y_%H-%M-%S"))
    with open(outFilename,'a+') as f:

	# section _____________________________________________________
        f.write(sectionLine)
	# subsection --------------------------------------------------
        f.write(formatSectionTitle('Model specifications'))


        # begin layer scheme
        f.write('Scheme of the model and its layer parameters:\n')
        f.write('\n')

        # begin by writing out input tensor
        f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format('INPUT TENSOR'),wLayerBox))

        for layer in model.layers:
            if isinstance(layer, layers.Dense):
                # count layer type
                nDenseLayers += 1

                # determine activation function
                if layer.activation == None:
                    activation = '-'
                elif layer.activation == relu:
                    activation = 'ReLU'
                elif layer.activation == selu:
                    activation = 'SELU'
                else:
                    # this could be done vor all of them but I prefer
                    # the selectively capitalized versions
                    activation = layer.activation.__name__


                # determine regularizer
                if layer.kernel_regularizer == None:
                    regularizer = '-'
                    reg_param = ''
                else:
                    if tf.__version__[2] >= '1':
                        # works only for regularizers with 1 regularization parameter
                        regularizer = list(layer.kernel_regularizer.get_config().keys())[0]
                        reg_param = list(layer.kernel_regularizer.get_config().values())[0] 
                        reg_param = str(reg_param)[:6]
                    else:
                        regularizer = type(layer.kernel_regularizer).__name__
                        if hasattr(layer.kernel_regularizer,'l1'):
                            reg_param = layer.kernel_regularizer.l1
                        if hasattr(layer.kernel_regularizer,'l2'):
                            reg_param = str(reg_param)
                            reg_param += ' ' + str(layer.kernel_regularizer.l2)
                        elif hasattr(layer.kernel_regularizer,'l'):
                            reg_param = layer.kernel_regularizer.l
                        else:
                            reg_param = ''

                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))
                #f.write(' {0:^{1}} \n'.format('\                  /',wLayerBox))

                # draw layer box
                with drawLayerBox(f, 'DENSE', layer.name):
                    f.write('|{0:^{1}}|\n'.format("neurons: {}".format(str(layer.units)),wLayerBox))
                    f.write('|{0:^{1}}|\n'.format("activation function: {}".format(activation),wLayerBox))
                    f.write('|{0:^{1}}|\n'.format("regularizer: {0} {1}".format(regularizer,reg_param),wLayerBox))

            elif isinstance(layer, layers.Conv2D) or isinstance(layer, layers.Conv1D):
                nConvLayers += 1

                # determine activation function
                if layer.activation == None:
                    activation = '-'
                elif layer.activation == relu:
                    activation = 'ReLU'
                elif layer.activation == selu:
                    activation = 'SELU'
                else:
                    # this could be done vor all of them but I prefer
                    # the selectively capitalized versions
                    activation = layer.activation.__name__
                
                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))

                # draw layer box
                with drawLayerBox(f, 'CONVOLUTION', layer.name):
                    f.write('|{0:^{1}}|\n'.format("number of kernels: {}".format(str(layer.filters)),wLayerBox))
                    f.write('|{0:^{1}}|\n'.format("shape: {0}  &  stride: {1}".format(str(layer.kernel_size),str(layer.strides)),wLayerBox))
                    f.write('|{0:^{1}}|\n'.format("activation function: {}".format(activation),wLayerBox))

            # deviation from "if isinstance" structuting choice
            # due to larger number of possible pooling layer
            elif 'pool' in layer._name:   

                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))

                # draw layer box
                with drawLayerBox(f, 'POOLING', layer.name):
                    f.write('|{0:^{1}}|\n'.format("pooling window shape: {}".format(str(layer.pool_size)),wLayerBox))
    

            elif isinstance(layer, layers.Flatten):
                nFlattenLayers += 1

                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))


                # draw layer box
                with drawLayerBox(f, 'FLATTEN', layer.name):
                    pass

            elif isinstance(layer, layers.Dropout):
                nDropoutLayers += 1

                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))

                # draw layer box
                with drawLayerBox(f, 'DROPOUT', layer.name):
                    f.write('|{0:^{1}}|\n'.format("dropped fraction: {}".format(str(layer.rate)),wLayerBox))

            # just box with layer type and name
            else:
                dataShape = layer.input.shape 
                dataShape = str(dataShape)
                # replace 'None' with 'batch'
                dataShape = dataShape.replace('None','batch')
                # write out shape of data tensor that is being passed
                # to the next layer
                f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))

                layerType = str(type(layer).__name__)
                # all caps
                layerType = layerType.upper()

                # draw layer box
                with drawLayerBox(f, layerType, layer.name):
                    pass

        # end by writing out output tensor
        dataShape = model.layers[-1].output.shape
        dataShape = str(dataShape)
        # replace 'None' with 'batch'
        dataShape = dataShape.replace('None','batch')
        # write it out
        f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format(dataShape),wLayerBox))
        f.write(' {0:^{1}} \n'.format('||{0:^19}||'.format('OUTPUT TENSOR'),wLayerBox))
        
        f.write('\n')
        f.write('In summary:\n')
        f.write('\n')

        f.write(formatOutput('Number of Dense Layers',nDenseLayers))
        f.write(formatOutput('Number of Conv Layers',nConvLayers))
        f.write(formatOutput('Number of Dropout Layers',nDropoutLayers))
        f.write(formatOutput('Number of Flatten Layers',nFlattenLayers))
        optimizer = model.optimizer._name
        lossFunction = model.loss
        f.write(formatOutput('Optimizer',optimizer))
        f.write(formatOutput('Loss function',lossFunction))
        
        f.write('\n')
        #model.summary(print_fn=lambda x: f.write(x + '\n'))

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def writeEvalSummary(score):
    """
    Creates an output file or extends an existing one.
    To be called from a run file after model evaluation.
    Writes summary of loss value & evaluation metrics of the model
    in the test phase.
    """

#    global outFilename # make sure to change the global variable
#    outFilename += '{}.out'.format(startTime.strftime("_%d-%m-%Y_%H-%M-%S"))

    with open(outFilename, 'a+') as f:
        f.write(sectionLine)
        f.write(formatSectionTitle('Model evaluation'))
        
        for name, value in score.items():
            f.write('{0:{2}} {1:{3}}\n'.format(name, str(value),wHeadLeft,wHeadRight))


#-------------------------------------------------------------------
#-------------------------------------------------------------------

def finalize():
    """
    Adds the header to the summary file. Included as separate function 
    so that it doe not matter in what order the different summary functions
    are called.
    """

    if inp.RunTitle == None:
        runTitle = '-'
    else:
        runTitle = inp.RunTitle

    with open(outFilename, 'r') as f:
        contents = f.readlines()

    newLines = ''
    newLines += '          ______________________________  \n'
    newLines += '        ||                              ||\n'
    newLines += '        ||     UNIQORN RUN SUMMARY      ||\n'
    newLines += '        ||______________________________||\n'
    newLines += '\n'
    newLines += formatHeader('Run title',runTitle)
    newLines += formatHeader('Execution date',startTime.strftime("%d.%m.%Y"))
    newLines += '\n'
    newLines += 'Time spent on...\n'
    newLines +=  headLine
    newLines += formatHeader('...data loading',durationDataLoading)
    newLines += formatHeader('...training and evaluation',durationTraining)
    newLines += formatHeader('...visualization',durationVisualization)
    newLines +=  headLine
    newLines += formatHeader('Duration Total',durationTotal)
    newLines += '\n'

    # insert new lines at the top
    contents.insert(0, newLines)

    with open(outFilename, 'w') as f:
        f.writelines(contents)


#-------------------------------------------------------------------
#-------------------------------------------------------------------

# to draw layer box lines and name
@contextmanager
def drawLayerBox(openfile, layerType, layerName):
    openfile.write(' {0:{1}} \n'.format(boxLine,wLayerBox))
    openfile.write('|{0:{1}}|\n'.format('',wLayerBox))
    openfile.write('|{0:^{1}}|\n'.format("{0} layer -'{1}'".format(layerType,layerName),wLayerBox))
    yield
    openfile.write('|{0:{1}}|\n'.format(boxLine,wLayerBox))

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def getLogo():
    """
    Reads the logo out of the logo.txt file that is currently stored
    in the source directory. Centers the logo with respect to the 
    longest lines in the output generation.

    Return:
    ------
    image : String type, centered UNIQORN logo image
    """
   
    # set pathToSource 
    global pathToSource
    pathToSource=Set_pathToSource()

    # check whether the given path ends in '/' or not
    if pathToSource[-1] != '/':
        pathToSource = pathToSource + '/'
    with open(pathToSource+'logo.txt', 'r') as f:
        alllines = f.readlines()
        image = ''
        for line in alllines:
            line = '{:<71}'.format(line.rstrip())
            line = '{0:^{1}}\n'.format(line,wSectionLine)
            image += line
    return image

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def formatOutput(leftText, rightText):
    """
    Applies a 2-column based format of fixed column widths to the given 
    input arguments.
    Column widths are defined at the top the module.

    Arguments:
    ----------
    leftText: whatever is to be put in the left column, before the ':'
    rightText: whatever is to be put in the right column, after the ':'

    Returns:
    --------
    headerString:  String type, formatted line of left + right text

    """
    # covert input arguments to string, just to be save
    leftText = str(leftText)
    rightText = str(rightText)

    outString = '{0:{2}} {1:{3}}\n'.format(leftText+':',rightText,wLeftCol,wRightCol)

    return outString

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def formatHeader(leftText, rightText):
    """
    Applies a 2-column based format of fixed column widths to the given 
    input arguments. Column widths differ from formatOutput.
    Column widths are defined at the top the module.

    Arguments:
    ----------
    leftText: whatever is to be put in the left column, before the ':'
    rightText: whatever is to be put in the right column, after the ':'

    Returns:
    --------
    headerString:  String type, formatted line of left + right text

    """
    # covert input arguments to string, just to be save
    leftText = str(leftText)
    rightText = str(rightText)

    headerString = '{0:{2}} {1:{3}}\n'.format(leftText+':',rightText,wHeadLeft,wHeadRight)

    return headerString

#-------------------------------------------------------------------
#-------------------------------------------------------------------

def formatSectionTitle(sectionName):
    """
    Creates the formatted title output with the (sub)section title name
    in between two lines.
    Line type is defined at the top the module.

    Arguments:
    ----------
    sectionName: String type, whatever is to be put in the title format

    Returns:
    --------
    sectionNameFormatted:  String type, formatted version of sectionName
    """

    sectionNameFormatted = ''
    sectionNameFormatted += '\n'
    sectionNameFormatted += headingLine
    sectionNameFormatted += '{}\n'.format(sectionName)
    sectionNameFormatted += headingLine
    sectionNameFormatted += '\n'

    return sectionNameFormatted
